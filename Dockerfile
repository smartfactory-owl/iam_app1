FROM python:3.9 AS base

RUN pip config set --global global.extra-index-url https://www.piwheels.org/simple

ENV HOME /root

RUN python -m venv /opt/venv
# Make sure we use the virtualenv:
ENV PATH="/opt/venv/bin:$PATH"

RUN apt update && apt install -y --no-install-recommends --allow-downgrades \
    bash \
    ca-certificates \
    git \
    build-essential \
    wget \
    nano \
    && rm -rf /var/lib/apt/lists/*
RUN python3 -m pip install --no-cache-dir -U \
    pip \
    setuptools \ 
    wheel
RUN git clone https://gitlab.com/smartfactory-owl/iam_app1

WORKDIR /iam_app1

RUN pip install -r requirements.txt

FROM python:3.9-slim AS final
COPY --from=base /opt/venv /opt/venv
COPY --from=base /iam_app1 /iam_app1
ENV PYTHONPATH="/usr/local/lib/python" 
ENV PATH="/opt/venv/bin:$PATH"
WORKDIR /iam_app1

RUN mv hblcfg_sample.json hblcfg.json

EXPOSE 8081

ENTRYPOINT ["python3"]

CMD ["hbloader.py"]


