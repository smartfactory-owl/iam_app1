#! /usr/bin/env python3

import sys
import os
import asyncio
import aiohttp
import json
import re
import pathlib
#import logging
import random
from time import time
from getpass import getpass
from hbloader_gui.gui import Hbloader_gui
from aiohttp import web

HBLCFG = 'hblcfg.json'
hblcfg_path = ''
cached_stamp = 0

def result_callback(result):
    print("Result:   {}".format('SUCCESSFUL' if result == 0 else 'FAILED' ))

def step_callback(percentage, message):
    print("Progress: {:>3}% - {}".format(percentage, message))

def main():
    #save last updated timestamp to CACHED_STAMP
    hblcfg_path = pathlib.Path(HBLCFG)
    if hblcfg_path.exists():
        #get absolute path
        hblcfg_path = hblcfg_path.resolve()
    global cached_stamp
    cached_stamp = os.stat(HBLCFG).st_mtime
    
    #create watchdog task
    loop = asyncio.get_event_loop()
    loop.create_task(config_watchdog())


    while True:
        #create maintask
        maintask = loop.create_task(hbloader_gui())
        loop.run_forever()
        #maintask.cancel()

async def config_watchdog():
    """
    Checks if the config file hblcfg.json has changed
    restarting hbloader asyncio Task
    """
    global cached_stamp
    loop = asyncio.get_event_loop()
    while True:
        stamp = os.stat(HBLCFG).st_mtime
        if stamp != cached_stamp:
            cached_stamp = stamp
            print("New Config was found! Restarting hbloader...")
            #remove loggers
            #loggers = logging.getLogger()
            #while loggers.hasHandlers():
             #   loggers.removeHandler(loggers.handlers[0])
            loop.stop()
        await asyncio.sleep(10)

async def hbloader_gui():
    """
    starts aiohttp webserver
    """
    gui = Hbloader_gui()
    app = await gui.get_app()
    runner = web.AppRunner(app, access_log=None)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', 8081)
    await site.start()

def parse_envs(config):
    """
    parses environment variables and saves them to config
    """
    random_id = random.choice(range(10,100))
    timestamp = int(time())
    config['ip'] = os.environ.get('IP_ADDRESS', '127.0.0.1')
    config['host'] = config['ip']
    port = (int)(os.environ.get('PORT','8080'))
    if (port in range(1023, 65535)) or (port in (80,443)):
        config['port'] = str(port)
    else:
        config['port'] = '443'
    config['ssl'] = os.environ.get('SSL', 'True').lower() in ('true', '1', 't')
    config['target_name'] = os.environ.get('TARGET_NAME', 'unnamed_target_{}'.format(timestamp))
    config['controller_id'] = os.environ.get('CONTROLLER_ID', '{}{}'.format(timestamp,random_id))
    config['tenant_id'] = os.environ.get('TENANT_ID','default')
    config['login'] = os.environ.get('USER', 'admin')
    config['password'] = os.environ.get('PASSWORD', 'admin')
    config['log_level'] = os.environ.get('LOG_LEVEL', 'INFO')
    config['run_as_Service'] = os.environ.get('RUN_AS_SERVICE', 'no')
    config['docker_registry'] = os.environ.get('DOCKER_REGISTRY', 'default')
    config['single_app_mode'] = os.environ.get('SINGLE_APP_MODE', 'False').lower() in ('true', '1', 't')


def load_config():
    """
    Load configuration params
    If file exists load it as params,
    if not create it with default parameters
    """
    config = {
    "ssl" : False,
    "host" :"127.0.0.1",
    "tenant_id" : "default",
    "target_name" : "",
    "login" : "admin",
    "password" : "admin",
    "auth_token" : "",
    "attributes" : {"MAC": ""},
    "loglevel" : "INFO",
    "run_as_service" : "no",
    "port" :  "443",
    "docker_registry": "default",
    "single_app_mode": True
    }

    """
    if config file exist and contains host ip 
    return this config 
    """
    if pathlib.Path(HBLCFG).exists():
        with open(HBLCFG, "r") as config_file:
            config = json.load(config_file)

        if config["ip"]:
            return config

    #parse environment variables
    parse_envs(config)
    #save and return updated config
    with open(HBLCFG, "w") as config_file:
            json.dump(config, config_file, indent=4)

    return config

if __name__ == "__main__":
    main()
